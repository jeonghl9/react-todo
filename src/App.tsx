import React from 'react';
import TodoApp from './containers/TodoApp';
import { createStore } from 'redux';
import rootReducer from './modules';
import { Provider } from 'react-redux';

const App: React.FC = () => {
  const store = createStore(rootReducer)
  return <Provider store={store}>
            <TodoApp />
         </Provider>
};

export default App;